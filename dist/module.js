'use strict';

System.register(['./treemap_ctrl'], function (_export, _context) {
  "use strict";

  var TreemapCtrl;
  return {
    setters: [function (_treemap_ctrl) {
      TreemapCtrl = _treemap_ctrl.TreemapCtrl;
    }],
    execute: function () {
      _export('PanelCtrl', TreemapCtrl);
    }
  };
});
//# sourceMappingURL=module.js.map
