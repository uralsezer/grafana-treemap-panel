import _ from 'lodash';
import appEvents from 'app/core/app_events';

export default function link(scope, elem, attrs, ctrl) {

  function emit(field, value) {
    appEvents.emit('add-selection', {
      field: field,
      value: value
    });
  }


  var data, panel, svgWrapper, highlight_text;
  var tooltipEle = elem.find('.tooltip');
  var captionEle = elem.find('.caption');

  var theme = grafanaBootData.user.lightTheme ? '-light' : '-dark';

  // elem = elem.find('.networkchart-panel');

  var $panelContainer = elem.find('.panel-container');

  ctrl.events.on('render', function () {
    render();
  });
  data = ctrl.data;
  panel = ctrl.panel;

  var gaugeByClass = elem.find('.grafana-d3-treemap');
  //gaugeByClass.append('<center><div id="'+ctrl.containerDivId+'"></div></center>');
  gaugeByClass.append('<div id="' + ctrl.containerDivId + '"></div>');
  var container = gaugeByClass[0].childNodes[0];
  ctrl.setContainer(container);
  if ($('#' + panel.treemapDivId).length) {
    $('#' + panel.treemapDivId).remove();
  }

  function setElementHeight() {
    try {
      return true;
    } catch (e) { // IE throws errors sometimes
      return false;
    }
  }


  function showError(errorText) {
    var noData = elem.find(".no-data");
    if (errorText) {
      noData.text(errorText);
      noData.show();
    }
    else
      noData.hide();
  }


  function y(d, i) {
    return 25 * (i + 1)
  }


  var tooltipEvals = [];

  function parseTooltip(tooltip, columnTexts) {
    var regExp = /{{(.*?)}}/g;
    var tooltipEval = ctrl.$sanitize(tooltip);
    var match;
    do {
      match = regExp.exec(tooltip);
      if (match) {
        var index = columnTexts.indexOf(match[1]);
        var replaceWith = index !== -1 ? `d[${index}]` : "";

        tooltipEval = tooltipEval.replace(match[1], replaceWith)
      }
    } while (match);

    return tooltipEval;
  }

  function getPanelWidthBySpan() {
    var trueWidth = 0;
    if (typeof panel.span === 'undefined') {
      // get the width based on the scaled container (v5 needs this)
      trueWidth = $panelContainer[0].clientWidth;
    } else {
      // v4 and previous used fixed spans
      var viewPortWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      // get the pixels of a span
      var pixelsPerSpan = viewPortWidth / 12;
      // multiply num spans by pixelsPerSpan
      trueWidth = Math.round(panel.span * pixelsPerSpan);
    }
    return trueWidth;
  }

  const strToColor = str => {
    let hash = 0;
    str = str + "/";
    for (let i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    let colour = '#';
    for (let i = 0; i < 3; i++) {
      const value = (hash >> (i * 8)) & 0xff;
      colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
  };


  function addTreemap(dataList) {

    function checkHighlight(d) {
      return ctrl.highlight_text && d.toLowerCase().indexOf(ctrl.highlight_text) !== -1;
    }

    if (typeof d3 == 'undefined')
      return;

    var defaults = {
      margin: {top: 24, right: 8, bottom: 10, left: 0},
      rootname: "TOP",
      format: ".2f",
      title: "",
      width: 960,
      height: 500
    };
    let o = {};
    var root,
      opts = $.extend(true, {}, defaults, o),
      formatNumber = d3.format(opts.format),
      rname = opts.rootname,
      margin = opts.margin,
      theight = 36 + 16;
    var width = getPanelWidthBySpan() - 10;
    var height = elem.height() - theight;

    d3.select('#' + ctrl.containerDivId).selectAll('svg').remove();
    var w = width, h = height;
    var transitioning;
    var color = d3.scale.category20c();

    // var fill = d3.scale.ordinal()
    //   .range(['#eca1a6','#7a3b2e', '#d6cbd3','#454140','#ff7b25','#d64161','#feb236','#6b5b95','#bdcebe',
    //     "#023fa5", "#7d87b9", "#bec1d4", "#d6bcc0",'#a79e84', "#bb7784", "#8e063b", "#4a6fe3", "#8595e1", "#b5bbe3",
    //     "#e6afb9",'#f7786b', "#e07b91",'#ada397', "#d33f6a", "#11c638", "#8dd593",'#92a8d1', "#c6dec7", "#ead3c6", "#f0b98d", "#ef9708", "#0fcfc0",
    //     "#9cded6", "#d5eae7", "#f3e1eb", "#f6c4e1",'#bd5734', "#f79cd4",'#034f84']);
    var x = d3.scale.linear()
      .domain([0, w])
      .range([0, w]);

    var y = d3.scale.linear()
      .domain([0, h])
      .range([0, h]);
    var treemap = d3.layout.treemap()
      .children(function (d, depth) {
        return depth ? null : d._children;
      })
      .sort(function (a, b) {
        return a.value - b.value;
      })
     // .ratio(height / width * 0.5 * (1 + Math.sqrt(5)))
      .round(false);
    var svg = d3.select(panel.svgContainer).append("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("class", "d3-treemap")
      .style("margin-left", -margin.left + "px")
      .style("margin-right", margin.right + "px")
      .style("margin-bottom", margin.bottom + "px")
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
      .style("shape-rendering", "crispEdges");
    var grandparent = svg.append("g")
      .attr("class", "grandparent");

    grandparent.append("rect")
      .attr("y", -margin.top)
      .attr("width", width)
      .attr("height", margin.top);

    grandparent.append("text")
      .attr("x", 6)
      .attr("y", 6 - margin.top)
      .attr("dy", ".75em");

    // if (opts.title) {
    //   $("#chart").prepend("<p class='title'>" + opts.title + "</p>");
    // }
    if (dataList instanceof Array) {
      root = {key: rname, values: dataList};
    } else {
      root = dataList;
    }

    initialize(root);
    accumulate(root);
    layout(root);
    display(root);
    function initialize(root) {
      root.x = root.y = 0;
      root.dx = width;
      root.dy = height;
      root.depth = 0;
    }
    function accumulate(d) {
      return (d._children = d.values)
        ? d.value = d.values.reduce(function(p, v) { return p + accumulate(v); }, 0)
        : d.value; 
    }
    function layout(d) {
      if (d._children) {
        treemap.nodes({_children: d._children});
        d._children.forEach(function(c) {
          c.x = d.x + c.x * d.dx;
          c.y = d.y + c.y * d.dy;
          c.dx *= d.dx;
          c.dy *= d.dy;
          c.parent = d;
          layout(c);
        });
      }
    }

    function display(d) {
      grandparent
        .datum(d.parent)
        .on("click", transition)
        .select("text")
        .text(name(d));

      var g1 = svg.insert("g", ".grandparent")
        .datum(d)
        .attr("class", "depth");

      var g = g1.selectAll("g")
        .data(d._children)
        .enter().append("g");

      g.filter(function(d) { return d._children; })
        .classed("children", true)
        .on("click", transition);

      var children = g.selectAll(".child")
        .data(function(d) { return d._children || [d]; })
        .enter().append("g");

      children.append("rect")
        .attr("class", "child")
        .call(rect)
        .append("title")
        .text(function(d) { return d.key + " (" + formatNumber(d.value) + ")"; });
      children.append("text")
        .attr("class", "ctext")
        .text(function(d) { return d.key; })
        .call(text2);

      g.append("rect")
        .attr("class", "parent")
        .call(rect);

      var t = g.append("text")
        .attr("class", "ptext")
        .attr("dy", ".75em")

      t.append("tspan")
        .text(function(d) { return d.key; });
      t.append("tspan")
        .attr("dy", "1.0em")
        .text(function(d) { return formatNumber(d.value); });
      t.call(text);

      g.selectAll("rect")
        .style("fill", function(d) { return strToColor(d.key); });

      function transition(d) {
        if (transitioning || !d) return;
        transitioning = true;

        var g2 = display(d),
          t1 = g1.transition().duration(750),
          t2 = g2.transition().duration(750);

        // Update the domain only after entering new elements.
        x.domain([d.x, d.x + d.dx]);
        y.domain([d.y, d.y + d.dy]);

        // Enable anti-aliasing during the transition.
        svg.style("shape-rendering", null);

        // Draw child nodes on top of parent nodes.
        svg.selectAll(".depth").sort(function(a, b) { return a.depth - b.depth; });

        // Fade-in entering text.
        g2.selectAll("text").style("fill-opacity", 0);

        // Transition to the new view.
        t1.selectAll(".ptext").call(text).style("fill-opacity", 0);
        t1.selectAll(".ctext").call(text2).style("fill-opacity", 0);
        t2.selectAll(".ptext").call(text).style("fill-opacity", 1);
        t2.selectAll(".ctext").call(text2).style("fill-opacity", 1);
        t1.selectAll("rect").call(rect);
        t2.selectAll("rect").call(rect);

        // Remove the old node when the transition is finished.
        t1.remove().each("end", function() {
          svg.style("shape-rendering", "crispEdges");
          transitioning = false;
        });
      }

      return g;
    }

    function text(text) {
      text.selectAll("tspan")
        .attr("x", function(d) { return x(d.x) + 6; })
      text.attr("x", function(d) { return x(d.x) + 6; })
        .attr("y", function(d) { return y(d.y) + 6; })
        .style("opacity", function(d) { return this.getComputedTextLength() < x(d.x + d.dx) - x(d.x) ? 1 : 0; });
    }

    function text2(text) {
      text.attr("x", function(d) { return x(d.x + d.dx) - this.getComputedTextLength() - 6; })
        .attr("y", function(d) { return y(d.y + d.dy) - 6; })
        .style("opacity", function(d) { return this.getComputedTextLength() < x(d.x + d.dx) - x(d.x) ? 1 : 0; });
    }

    function rect(rect) {
      rect.attr("x", function(d) { return x(d.x); })
        .attr("y", function(d) { return y(d.y); })
        .attr("width", function(d) { return x(d.x + d.dx) - x(d.x); })
        .attr("height", function(d) { return y(d.y + d.dy) - y(d.y); });
    }

    function name(d) {
      return d.parent
        ? name(d.parent) + " / " + d.key + " (" + formatNumber(d.value) + ")"
        : d.key + " (" + formatNumber(d.value) + ")";
    }

    if (ctrl.highlight_text) {
      allTheGroups.transition().style("font-size", function (d) {
        return checkHighlight(rdr(d).gname) ? "15px" : "0px";
      });
      allArcs.transition()
        .duration(1000)
        .delay(2000)
        .style("stroke", "black")
        .style("fill", function (d) {
          return fill(rdr(d).gname);
        })
        .attr("d", function (d) {
          return checkHighlight(rdr(d).gname) ? highlightedArc(d) : arc(d)
        })
        .transition()
        .duration(1000)
        .delay(10000)
        .style("stroke", function (d) {
          return checkHighlight(rdr(d).gname) ? "lightblue" : "black"
        })
        .attr("d", arc)

    }
  }


  function render() {
    data = ctrl.data;
    panel = ctrl.panel;
    if (setElementHeight())
      if (ctrl._error || !data || data.rows.length === 0) {

        showError(ctrl._error || "No data points");

        data = [];
        //addTreemap(data);

      }
      else {
        let dataList2 = mapData(data, panel.detangle.fileBased);
        addTreemap(dataList2);
        showError(false);
      }


    ctrl.renderingCompleted();
  }

  function mapData(dataList, fileBased) {
    let modifiedDataList = [];
    const pathColumn = '@path';
    const fileColumn = '@file';
    const apiPathColumn = 'path';
    const apiRefColumn = 'ref';
    let tempColumns = dataList.columns;
    let tempRows = dataList.rows;
    let tempFilePathIndex = getIndex(pathColumn, tempColumns);
    if (tempFilePathIndex < 0) {
      tempFilePathIndex = getIndex(fileColumn, tempColumns);
    }
    if (tempFilePathIndex < 0) {
      tempFilePathIndex = getIndex(apiPathColumn, tempColumns);
    }
    if (tempFilePathIndex < 0) {
      tempFilePathIndex = getIndex(apiRefColumn, tempColumns);
    }


    let exampleData = tempRows[0][tempFilePathIndex];


    let maxLevel = (exampleData.match(/\//g) || []).length - 1;
    if (fileBased) {
      tempRows.forEach(tempRow => {
        let tempPath = tempRow[tempFilePathIndex];
        let tempLevel = (tempPath.match(/\//g) || []).length - 1;
        if (tempLevel > maxLevel) {
          maxLevel = tempLevel;
        }
      });
    }
    tempRows.forEach(tempRow => {
      let tempPath = tempRow[tempFilePathIndex];
      let tempValue = tempRow[tempColumns.length - 1];
      let tempObject = {
        "key": tempPath,
        "value": tempValue
      };

      for (let i = maxLevel; i > 0; i--) {
        tempObject[i] = tempPath.substring(0, nthIndex(tempPath, '/', i));
      }

      modifiedDataList.push(tempObject);
    });
    var levels = Array.from(new Array(maxLevel),(val,index)=>index + 1);;
    var nest = d3.nest();
    for (var i = 0; i < levels.length; i++) {
      nest = nest.key( createNestingFunction(levels[i]) );
    }

    return nest.entries(modifiedDataList);
  }

  function createNestingFunction(propertyName){
    return function(d){
      return d[propertyName];
    };
  }

  function nthIndex(str, pat, n){
    var L= str.length, i= -1;
    while(n-- && i++<L){
      i= str.indexOf(pat, i);
      if (i < 0) break;
    }
    return i;
  }

  function getIndex(text, columnList) {
    return _.findIndex(columnList, { text: text });
  }


}

